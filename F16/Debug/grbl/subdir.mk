################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../grbl/coolant_control.c \
../grbl/eeprom.c \
../grbl/gcode.c \
../grbl/jog.c \
../grbl/limits.c \
../grbl/motion_control.c \
../grbl/nuts_bolts.c \
../grbl/planner.c \
../grbl/print.c \
../grbl/probe.c \
../grbl/protocol.c \
../grbl/report.c \
../grbl/serial.c \
../grbl/settings.c \
../grbl/spindle_control.c \
../grbl/stepper.c \
../grbl/system.c 

OBJS += \
./grbl/coolant_control.o \
./grbl/eeprom.o \
./grbl/gcode.o \
./grbl/jog.o \
./grbl/limits.o \
./grbl/motion_control.o \
./grbl/nuts_bolts.o \
./grbl/planner.o \
./grbl/print.o \
./grbl/probe.o \
./grbl/protocol.o \
./grbl/report.o \
./grbl/serial.o \
./grbl/settings.o \
./grbl/spindle_control.o \
./grbl/stepper.o \
./grbl/system.o 

C_DEPS += \
./grbl/coolant_control.d \
./grbl/eeprom.d \
./grbl/gcode.d \
./grbl/jog.d \
./grbl/limits.d \
./grbl/motion_control.d \
./grbl/nuts_bolts.d \
./grbl/planner.d \
./grbl/print.d \
./grbl/probe.d \
./grbl/protocol.d \
./grbl/report.d \
./grbl/serial.d \
./grbl/settings.d \
./grbl/spindle_control.d \
./grbl/stepper.d \
./grbl/system.d 


# Each subdirectory must supply rules for building sources it contributes
grbl/coolant_control.o: ../grbl/coolant_control.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/coolant_control.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/eeprom.o: ../grbl/eeprom.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/eeprom.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/gcode.o: ../grbl/gcode.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/gcode.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/jog.o: ../grbl/jog.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/jog.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/limits.o: ../grbl/limits.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/limits.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/motion_control.o: ../grbl/motion_control.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/motion_control.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/nuts_bolts.o: ../grbl/nuts_bolts.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/nuts_bolts.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/planner.o: ../grbl/planner.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/planner.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/print.o: ../grbl/print.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/print.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/probe.o: ../grbl/probe.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/probe.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/protocol.o: ../grbl/protocol.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/protocol.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/report.o: ../grbl/report.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/report.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/serial.o: ../grbl/serial.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/serial.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/settings.o: ../grbl/settings.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/settings.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/spindle_control.o: ../grbl/spindle_control.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/spindle_control.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/stepper.o: ../grbl/stepper.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/stepper.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"
grbl/system.o: ../grbl/system.c
	arm-none-eabi-gcc -c "$<" -mcpu=cortex-m3 -std=gnu11 -g3 -DUSE_FULL_LL_DRIVER '-D__weak=__attribute__((weak))' '-D__packed=__attribute__((__packed__))' -DUSE_HAL_DRIVER -DSTM32F103xB -DSTM32 -DSTM32F1 -DSTM32F16 -DSTM32F1_6 -c -I../Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc -I../Drivers/STM32F1xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F1xx/Include -I../Drivers/CMSIS/Include -I../stm32 -I../grbl -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"grbl/system.d" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

